(function() {
    
    var FilmModule = angular.module("FilmModule", []);
    
    var __FilmFac = function($http, $q) {
        var serviceObject = {
            getAllFilms: function() {
                //As __FilmSvc
                
            },
            getFilm: function(movidId) {
                //As __FilmSvc

            }
        };
        return (serviceObject);
    };
    
    var __FilmSvc = function($http, $q) {
        
        this.getAllFilms = function() {
            var defer = $q.defer();
            $http.get("/api/films")
                .then(function(result) {
                    //Good 
                    defer.resolve(result.data);
                }).catch(function(error) {
                    defer.reject(error);
                });
            return (defer.promise);
        };

        this.getFilm = function(movieId) {
            if (!movieId)
                return ($q.reject("Missing movie id"));
            
            var defer = $q.defer();
            $http.get("/api/film/" + movieId)
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                })
            
            return (defer.promise);
        }
        
    }

    FilmModule.factory("FilmFac", ["$http", "$q", __FilmFac]);
    FilmModule.service("FilmSvc", ["$http", "$q", __FilmSvc]);
    
})();














